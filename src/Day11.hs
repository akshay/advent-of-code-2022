{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Day11 where

import Control.Applicative ((<|>))
import Control.Monad (replicateM)
import Data.Attoparsec.Text
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.List (sortOn)
import qualified Data.Text.IO as Text
import Lens
import State

day11_1 :: IO ()
day11_1 = do
  monkeys <- IntMap.fromList . zip [0 ..] <$> readInput
  let (afterRound20, _) = runState (replicateM 20 (monkeyRound (`div` 3))) monkeys
      inspectionCounts = sortOn negate . map itemsInspected $ IntMap.elems afterRound20
      monkeyBusiness = head inspectionCounts * head (tail inspectionCounts)
  putStrLn $ showState afterRound20
  putStrLn $ "Monkey business: " <> show monkeyBusiness

showState :: IntMap Monkey -> String
showState =
  let printMonkey i m = "Monkey " <> show i <> ": " <> "possesions=" <> show (m ^. items_) <> ", inspected=" <> show (m ^. itemsInspected_) <> "\n"
   in IntMap.foldMapWithKey printMonkey

monkeyRound :: (Item -> Item) -> State (IntMap Monkey) ()
monkeyRound f = do
  mapM_ (monkeyTurn f) =<< gets IntMap.keys

monkeyTurn :: (Item -> Item) -> Int -> State (IntMap Monkey) ()
monkeyTurn relief n = do
  m <- gets (IntMap.! n)
  mapM_ (inspectAndThrow relief m) (m ^. items_)
  modify
    ( IntMap.adjust
        ((items_ .~ []) . (itemsInspected_ +~ length (m ^. items_)))
        n
    )

inspectAndThrow :: (Item -> Item) -> Monkey -> Item -> State (IntMap Monkey) ()
inspectAndThrow relief m item = do
  let finalWorryLevel = relief $ operation m item
      runTest (DivisibleBy t) = finalWorryLevel `mod` t == 0
      throwTo =
        if runTest (test m)
          then successThrow m
          else failureThrow m
  modify (IntMap.adjust (items_ <>~ [finalWorryLevel]) throwTo)

day11_2 :: IO ()
day11_2 = do
  monkeys <- IntMap.fromList . zip [0 ..] <$> readInput
  -- Doing a (`mod` testGCD) ensures that the worry doesn't overflow, while
  -- ensuring results of all the tests is same. GCD is just the product here as
  -- all the tests look for divisibility by prime numbers.
  let testGCD = product $ map (unDivisibleBy . test) $ IntMap.elems monkeys
      (afterRound10000, _) = runState (replicateM 10000 (monkeyRound (`mod` testGCD))) monkeys
      inspectionCounts = sortOn negate . map itemsInspected $ IntMap.elems afterRound10000
      monkeyBusiness = head inspectionCounts * head (tail inspectionCounts)
  putStrLn $ showState afterRound10000
  putStrLn $ "Monkey business: " <> show monkeyBusiness

type Item = Int

data Monkey = Monkey
  { items :: [Item],
    itemsInspected :: Int,
    operation :: Item -> Item,
    test :: Test,
    successThrow :: Int,
    failureThrow :: Int
  }

newtype Test = DivisibleBy {unDivisibleBy :: Int}

items_ :: Lens' Monkey [Item]
items_ f m = fmap (\i' -> m {items = i'}) (f (items m))

itemsInspected_ :: Lens' Monkey Int
itemsInspected_ f m = fmap (\i' -> m {itemsInspected = i'}) (f (itemsInspected m))

monkeyParser :: Parser Monkey
monkeyParser =
  Monkey
    <$ string "Monkey "
    <* (decimal @Int)
    <* char ':'
    <* endOfLine
    <*> itemsParser
    <* endOfLine
    <*> pure 0
    <*> operationParser
    <* endOfLine
    <*> testParser
    <* endOfLine
    <*> successThrowParser
    <* endOfLine
    <*> failureThrowParser
    <* endOfLine

itemsParser :: Parser [Item]
itemsParser = do
  _ <- string "  Starting items: "
  decimal `sepBy` string ", "

data Operand = Old | Number Int

resolveOperand :: Int -> Operand -> Int
resolveOperand old Old = old
resolveOperand _ (Number n) = n

operationParser :: Parser (Item -> Item)
operationParser = do
  _ <- string "  Operation: new = "
  let operandParser = (id <$ string "old") <|> (const <$> decimal)
      operatorParser = ((*) <$ string " * ") <|> ((+) <$ string " + ")
  op1 <- operandParser
  f <- operatorParser
  op2 <- operandParser
  pure (\x -> f (op1 x) (op2 x))

testParser :: Parser Test
testParser = do
  _ <- string "  Test: divisible by "
  DivisibleBy <$> decimal

successThrowParser :: Parser Int
successThrowParser = do
  _ <- string "    If true: throw to monkey "
  decimal

failureThrowParser :: Parser Int
failureThrowParser = do
  _ <- string "    If false: throw to monkey "
  decimal

readInput :: IO [Monkey]
readInput = do
  input <- Text.getContents
  case parseOnly ((monkeyParser `sepBy` endOfLine) <* endOfInput) input of
    Left e -> error e
    Right ms -> pure ms
