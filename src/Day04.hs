module Day04 where

import Data.Attoparsec.Text
import qualified Data.Text.IO as Text

day4_1 :: IO ()
day4_1 = do
  pairs <- readInput
  let completeOverlaps = length $ filter (uncurry isCompleteOverlap) pairs
  putStrLn $ "Complete overlaps: " <> show completeOverlaps

isCompleteOverlap :: Assignment -> Assignment -> Bool
isCompleteOverlap (start1, end1) (start2, end2) =
  (start1 >= start2 && end1 <= end2) || (start2 >= start1 && end2 <= end1)

day4_2 :: IO ()
day4_2 = do
  pairs <- readInput
  let overlaps = length $ filter (uncurry isOverlap) pairs
  putStrLn $ "Overlaps: " <> show overlaps

isOverlap :: Assignment -> Assignment -> Bool
isOverlap ass1@(start1, end1) ass2@(start2, end2) =
  (start1 `isBetweeen` ass2)
  || (end1 `isBetweeen` ass2)
  || (start2 `isBetweeen` ass1)
  || (end2 `isBetweeen` ass1)

isBetweeen :: Int -> (Int, Int) -> Bool
isBetweeen n (a, b) = a <= n && n <= b

type Assignment = (Int, Int)

type Pair = (Assignment, Assignment)

assignmentParser :: Parser Assignment
assignmentParser =
  (,)
    <$> decimal
    <* char '-'
    <*> decimal

pairParser :: Parser Pair
pairParser =
  (,)
    <$> assignmentParser
    <* char ','
    <*> assignmentParser

readInput :: IO [Pair]
readInput = do
  inputText <- Text.getContents
  case parseOnly (sepBy1 pairParser endOfLine <* endOfInput) inputText of
    Left err -> error err
    Right pairs -> pure pairs
