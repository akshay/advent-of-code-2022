{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE TupleSections #-}

module State where

newtype State s a = State {runState :: s -> (s, a)}
  deriving (Functor)

instance Applicative (State s) where
  pure x = State (,x)
  State sf <*> State sx =
    State $ \s ->
      let (s', f) = sf s
          (s'', x) = sx s'
       in (s'', f x)

instance Monad (State s) where
  State sx >>= f = State $ \s ->
    let (s', x) = sx s
     in runState (f x) s'

-- >>> runState get 10
-- (10,10)
get :: State s s
get = State $ \s -> (s, s)

-- >>> runState (put 10) 1
-- (10,())
put :: s -> State s ()
put s = State $ const (s, ())

-- >>> runState (modify (+ 1)) 1
-- (2,())
-- >>> runState (do modify (+1); modify (* 2)) 1
-- (4,())
modify :: (s -> s) -> State s ()
modify f = State $ \s -> (f s, ())

gets :: (s -> a) -> State s a
gets f = f <$> get

execState :: State s a -> s -> a
execState s = snd . runState s
