module Day02 where

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import Data.Bifunctor (bimap)
import Data.Functor (($>))
import qualified Data.Text.IO as Text

day2_1 :: IO ()
day2_1 = do
  rounds <- map (bimap shapeFromABC shapeFromXYZ) <$> readInput
  let totalScore = sum $ map (uncurry calculateScore) rounds
  putStrLn $ "Total Score: " <> show totalScore

calculateScore :: Shape -> Shape -> Int
calculateScore opponentShape yourShape =
  shapeScore yourShape + outcomeScore (calculateOutcome opponentShape yourShape)

-- | @calculateOutcome opponentShape yourShape@
calculateOutcome :: Shape -> Shape -> Outcome
calculateOutcome Rock Paper = Win
calculateOutcome Paper Rock = Loss
calculateOutcome Paper Scissors = Win
calculateOutcome Scissors Paper = Loss
calculateOutcome Scissors Rock = Win
calculateOutcome Rock Scissors = Loss
calculateOutcome _ _ = Draw

shapeScore :: Shape -> Int
shapeScore Rock = 1
shapeScore Paper = 2
shapeScore Scissors = 3

outcomeScore :: Outcome -> Int
outcomeScore Loss = 0
outcomeScore Draw = 3
outcomeScore Win = 6

day2_2 :: IO ()
day2_2 = do
  roundWithOutcomes <- map (bimap shapeFromABC outcomeFromXYZ) <$> readInput
  let rounds = map (\(opponentShape, outcome) -> (opponentShape, calculateYourShape opponentShape outcome)) roundWithOutcomes
      totalScore = sum $ map (uncurry calculateScore) rounds
  putStrLn $ "Total Score: " <> show totalScore

calculateYourShape :: Shape -> Outcome -> Shape
calculateYourShape opponentShape Draw = opponentShape
calculateYourShape Rock Loss = Scissors
calculateYourShape Paper Loss = Rock
calculateYourShape Scissors Loss = Paper
calculateYourShape Rock Win = Paper
calculateYourShape Paper Win = Scissors
calculateYourShape Scissors Win = Rock

data Shape = Rock | Paper | Scissors

shapeFromABC :: ABC -> Shape
shapeFromABC A = Rock
shapeFromABC B = Paper
shapeFromABC C = Scissors

shapeFromXYZ :: XYZ -> Shape
shapeFromXYZ X = Rock
shapeFromXYZ Y = Paper
shapeFromXYZ Z = Scissors

data Outcome = Loss | Draw | Win

outcomeFromXYZ :: XYZ -> Outcome
outcomeFromXYZ X = Loss
outcomeFromXYZ Y = Draw
outcomeFromXYZ Z = Win


data ABC = A | B | C

data XYZ = X | Y | Z

abcParser :: Parser ABC
abcParser =
  char 'A' $> A
    <|> char 'B' $> B
    <|> char 'C' $> C

xyzParser :: Parser XYZ
xyzParser =
  char 'X' $> X
    <|> char 'Y' $> Y
    <|> char 'Z' $> Z

type InputLine = (ABC, XYZ)

lineParser :: Parser InputLine
lineParser =
  (,)
    <$> abcParser
    <* char ' '
    <*> xyzParser

inputParser :: Parser [InputLine]
inputParser = sepBy1 lineParser endOfLine

readInput :: IO [InputLine]
readInput = do
  inputText <- Text.getContents
  case parseOnly (inputParser <* endOfInput) inputText of
    Left err -> error err
    Right input -> pure input
