{-# LANGUAGE BinaryLiterals #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE RecordWildCards #-}

module Day17 where

import Data.Bits
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.List (foldl')
import Data.List.Extra (nubOrd)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Word (Word8)

day17_1 :: IO ()
day17_1 = do
  pushPattern <- readInput
  let shapePattern = [Dash, Plus, RevL, I, Box]
      shapes = take 2022 $ cycle shapePattern
      Env {..} = dropShapes shapes (Env (cycle pushPattern) 0 ground)
  putStrLn $ "Max Y: " <> show (chopped + maxY rested)

type Occupied = IntMap Word8

maxY :: Occupied -> Int
maxY = fst . IntMap.findMax

showMap :: Int -> Occupied -> String
showMap offset occ =
  let minX = 0
      maxX = 8
      minY = fst $ IntMap.findMin occ
      pixel 0 _ = '|'
      pixel 8 _ = '|'
      pixel x y =
        if flip testBit x $ IntMap.findWithDefault 0 (y - offset) occ
          then '#'
          else '.'
      l y = map (`pixel` (y + offset)) (reverse [minX .. maxX])
      ls = map l (reverse [minY .. maxY occ])
   in unlines ls

data Env = Env
  { pushes :: [Push],
    chopped :: Int,
    rested :: Occupied
  }

-- | (nextShape, nextPushId, occupied)
type SeenState = (Shape, Int, Occupied)

dropShapes :: [Shape] -> Env -> Env
dropShapes shapes env =
  foldl' (flip dropShape) env shapes

-- | Returns the state, first occurrance, heightAtFirstOccurrance, secondOccurance, heightAtSecondOccurance
dropShapesUntilRepeat :: [Shape] -> Env -> Map SeenState (Int, Int) -> Int -> (SeenState, Int, Int, Int, Int)
dropShapesUntilRepeat [] _ _ _ = error "no shapes left"
dropShapesUntilRepeat (s : ss) env states n
  | n > 20_000 = error "giving up"
  | otherwise =
      let state = (s, pushId . head $ pushes env, rested env)
          height = maxY (rested env) + chopped env
       in case Map.lookup state states of
            Nothing -> dropShapesUntilRepeat ss (dropShape s env) (Map.insert state (n, height) states) (n + 1)
            Just (nPrev, prevHeight) -> (state, nPrev, prevHeight, n, height)

ground :: Occupied
ground = IntMap.singleton 0 0xFF

mkEdges :: Shape -> (Int, Int) -> Occupied
mkEdges shape (x, y) = IntMap.fromList $ zip [y ..] (xsFor shape x)

dropShape :: Shape -> Env -> Env
dropShape shape env@(Env {..}) =
  case pushes of
    (p1 : p2 : p3 : p4 : prest) ->
      let startingPos = (foldl' (flip (resolvePush shape)) 5 [p1, p2, p3, p4], maxY rested + 1)
          edges = mkEdges shape startingPos
       in moveDown edges (env {pushes = prest})
    _ -> error "Not enough pushes"

resolvePush :: Shape -> Push -> Int -> Int
resolvePush _ (Push _ L) x =
  if x >= 7
    then x
    else x + 1
resolvePush s (Push _ R) x =
  if x - (width s - 1) <= 1
    then x
    else x - 1

width :: Shape -> Int
width Dash = 4
width Plus = 3
width RevL = 3
width I = 1
width Box = 2

pushAll :: Occupied -> Push -> Occupied -> Occupied
pushAll edges p rested = maybe edges (\e -> if bumpsIntoAnotherPiece rested e then edges else e) (sequence $ IntMap.map (push p) edges)

stepShape :: Occupied -> Env -> Env
stepShape edges env@Env {..} =
  case pushes of
    (p : ps) ->
      let newEdges = pushAll edges p rested
       in moveDown newEdges (env {pushes = ps})
    _ -> error "no pushes left!"

bumpsIntoAnotherPiece :: Occupied -> Occupied -> Bool
bumpsIntoAnotherPiece rested = IntMap.foldrWithKey (\y newXs acc -> acc || maybe False (\oldXs -> oldXs .&. newXs /= 0) (IntMap.lookup y rested)) False

moveDown :: Occupied -> Env -> Env
moveDown edges env@Env {..} =
  let newEdges = IntMap.mapKeys (subtract 1) edges
      shapes = Map.fromList $ [(s, [1 .. 7]) | s <- [minBound .. maxBound]]
   in if bumpsIntoAnotherPiece rested newEdges
        then
          let newRested = restAt edges rested
              (chopped', newRestedChopped) = impermiableTop (maxY newRested) (fst (IntMap.findMin newEdges)) shapes newRested
           in env
                { chopped = chopped + chopped',
                  rested = newRestedChopped
                }
        else stepShape newEdges env

restAt :: Occupied -> Occupied -> Occupied
restAt = IntMap.unionWith (.|.)

impermiableTop :: Int -> Int -> Map Shape [Int] -> Occupied -> (Int, Occupied)
impermiableTop top minTop shapes rested
  | top < 0 = error $ "Top  < 0: " <> show top
  | top < minTop = (0, rested)
  | otherwise =
      -- let combos = [(s, ps) | s <- [minBound :: Shape .. maxBound], ps <- pushCombinations]
      let topRow = IntMap.findWithDefault 0 top rested
          -- allShapes = [minBound :: Shape .. maxBound]
          passed = Map.filter (not . null) $ Map.mapWithKey (canPassThrough topRow) shapes
       in if Map.null passed
            then (top, IntMap.mapKeys (subtract top) $ IntMap.filterWithKey (\k _ -> k >= top) rested)
            else impermiableTop (top - 1) minTop (withLeftAndRightMoves (top - 1) rested passed) rested

withLeftAndRightMoves :: Int -> Occupied -> Map Shape [Int] -> Map Shape [Int]
withLeftAndRightMoves y rested =
  Map.mapWithKey (\s xs -> nubOrd $ concatMap (pushBottomLeft rested s y) xs)

pushBottomLeft :: Occupied -> Shape -> Int -> Int -> [Int]
pushBottomLeft rested s y x =
  let edges = mkEdges s (x, y)
      leftPushed = pushAll edges (Push 0 L) rested
      rightPushed = pushAll edges (Push 0 R) rested
      newBottomLeft = ((finiteBitSize (0 :: Word8) - 1) -) . countLeadingZeros . foldr (.|.) 0 . IntMap.elems
      news = [newBottomLeft leftPushed, newBottomLeft rightPushed]
   in news

-- >>> canPassThrough (0xFF :: Word8) Dash [1 .. 7]
-- []
-- >>> canPassThrough (0x00 :: Word8) Dash [1 .. 7]
-- [4,5,6,7]
canPassThrough :: FiniteBits a => a -> Shape -> [Int] -> [Int]
canPassThrough occupiedSpots shape =
  filter (canPassFrom occupiedSpots shape)

-- >>> canPassFrom (0xFF :: Word8) Dash 6
-- False
-- >>> canPassFrom (0x00 :: Word8) Dash 6
-- True
-- >>> canPassFrom (0x02 :: Word8) I 1
-- False
-- >>> canPassFrom (0x02 :: Word8) I 2
-- True
canPassFrom :: FiniteBits a => a -> Shape -> Int -> Bool
canPassFrom occupiedSpots shape bottomLeft =
  let bottomRight = bottomLeft - (width shape - 1)
   in (bottomRight >= 1) && not (any (testBit occupiedSpots) [bottomRight .. bottomLeft])

push :: (FiniteBits a) => Push -> a -> Maybe a
push (Push _ L) x =
  if testBit x (finiteBitSize x - 1)
    then Nothing
    else Just (shiftL x 1)
push (Push _ R) x =
  if testBit x 1
    then Nothing
    else Just (shiftR x 1)

data Shape = Dash | Plus | RevL | I | Box
  deriving (Show, Eq, Ord, Bounded, Enum)

-- >>> showMap $ IntMap.singleton 1 . head $ xsFor Dash 5
-- "|..####.|\n"
xsFor :: Shape -> Int -> [Word8]
xsFor s x = (`shiftR` (7 - x)) <$> leftMost s

leftMost :: Shape -> [Word8]
leftMost Dash = [0b1111_0000]
leftMost Plus =
  [ 0b0100_0000,
    0b1110_0000,
    0b0100_0000
  ]
leftMost RevL =
  -- Looks upside down because y = 0 is bottom
  [ 0b1110_0000,
    0b0010_0000,
    0b0010_0000
  ]
leftMost I =
  [ 0b1000_0000,
    0b1000_0000,
    0b1000_0000,
    0b1000_0000
  ]
leftMost Box =
  [ 0b1100_0000,
    0b1100_0000
  ]

day17_2 :: IO ()
day17_2 = do
  pushPattern <- readInput
  let shapePattern = [Dash, Plus, RevL, I, Box]
      repeatData@((_, nextPushId, occ), firstOcc, firstHeight, secondOcc, secondHeight) = dropShapesUntilRepeat (cycle shapePattern) (Env (cycle pushPattern) 0 ground) mempty 0
      periodLength = secondOcc - firstOcc
      periodGain = secondHeight - firstHeight
      pushStart = dropWhile (\p -> pushId p /= nextPushId) (cycle pushPattern)
      end = 1_000_000_000_000
      lastNShapes = take ((end - firstOcc) `mod` periodLength) (drop firstOcc (cycle shapePattern))
      Env _ finalChopped finalOcc = dropShapes lastNShapes (Env pushStart 0 occ)
      finalHeight = firstHeight + periodGain * ((end - firstOcc) `div` periodLength) + finalChopped + maxY finalOcc - maxY occ
  putStrLn $ "Repeat: " <> show repeatData
  putStrLn $ "Final height: " <> show finalHeight

data Push = Push {pushId :: Int, pushType :: PushType}
  deriving (Show)

data PushType = L | R
  deriving (Show)

readInput :: IO [Push]
readInput = do
  types <-
    map
      ( \case
          '>' -> R
          '<' -> L
          c -> error $ "Invalid Direction: " <> show c
      )
      <$> getLine
  pure $ zipWith Push [0 ..] types
