{-# LANGUAGE OverloadedStrings #-}

module Day16 where

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import Data.Function ((&))
import Data.List.Extra (maximumOn)
import Data.Map (Map)
import qualified Data.Map as Map
import qualified Data.Set as Set
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text
import qualified Debug.Trace as Debug
import Lens

day16_1 :: IO ()
day16_1 = do
  valves <- readInput
  let processedValves = compressPaths (removeUselessValves "AA" valves)
      (mp, _) = maxPresssureRelease2 (Map.delete "AA" processedValves) (processedValves Map.! "AA") 30
  putStrLn $ "Max pressure release: " <> show mp

compressPathsFor :: Text -> Map Text Valve -> Map Text Valve
compressPathsFor from vs =
  let orig = tunnels (vs Map.! from)
      newTs =
        Map.foldrWithKey
          ( \neighbour neighbourCost ->
              let nTunnels = (tunnels $ vs Map.! neighbour)
               in Map.unionWith min (Map.map (+ neighbourCost) (Map.delete from nTunnels))
          )
          orig
          orig
   in Map.insert from ((vs Map.! from) {tunnels = newTs}) vs

removeUselessValves :: Text -> Map Text Valve -> Map Text Valve
removeUselessValves forbidden orig =
  case Map.toList (Map.filterWithKey (\n v -> n /= forbidden && flowRate v == 0) orig) of
    [] -> orig
    ((uselessName, uselessValve) : _) ->
      removeUselessValves forbidden $ removeValve uselessName uselessValve orig

removeValve :: Text -> Valve -> Map Text Valve -> Map Text Valve
removeValve toRemove valveToRemove m =
  Map.mapWithKey
    ( \n v ->
        case Map.lookup toRemove (tunnels v) of
          Nothing -> v
          Just costToRemoved ->
            v & tunnels_ %~ (Map.unionWith min (Map.map (+ costToRemoved) . Map.delete n $ tunnels valveToRemove) . Map.delete toRemove)
    )
    $ Map.delete toRemove m

day16_2 :: IO ()
day16_2 = do
  valves <- readInput
  let processedValves = compressPaths (removeUselessValves "AA" valves)
      aaValve = processedValves Map.! "AA"
      closedValves = Map.delete "AA" processedValves
      valveCombos = Set.toList $ combinations $ Map.keysSet closedValves
      (mp, ass1, trail1, ass2, trail2) =
        maximum $
          map
            ( \(human, elephant) ->
                let (humanScore, humanTrail) = maxPresssureRelease2 (Map.restrictKeys closedValves human) aaValve 26
                    (elepantScore, elephantTrail) = maxPresssureRelease2 (Map.restrictKeys closedValves elephant) aaValve 26
                 in (humanScore + elepantScore, human, humanTrail, elephant, elephantTrail)
            )
            valveCombos
  putStrLn $ "Paths: " <> printValves processedValves
  putStrLn $ "Assig1: " <> show (Set.toList ass1)
  putStrLn $ "Trail1: " <> show trail1
  putStrLn $ "Assig2: " <> show (Set.toList ass2)
  putStrLn $ "Trail2: " <> show trail2
  putStrLn $ "Max pressure release: " <> show mp

printValves :: Map Text Valve -> String
printValves =
  let printValve n v suffix = Text.unpack n <> ": " <> show (Map.toList (tunnels v)) <> "\n" <> suffix
   in Map.foldrWithKey printValve ""

compressPaths :: Map Text Valve -> Map Text Valve
compressPaths m =
  let n = Map.size m
      unconnected = Map.keysSet (Map.filter (\v -> Map.size (tunnels v) /= n - 1) m)
   in if Debug.traceShow (n, unconnected) $ Set.null unconnected
        then m
        else compressPaths $ Set.foldr compressPathsFor m unconnected

-- >>> Set.map (\(x,y) -> (Set.toList x, Set.toList y)) . combinations . Set.fromList $ [1,2,3,4]
-- fromList [([],[1,2,3,4]),([1],[2,3,4]),([1,2],[3,4]),([1,2,3],[4]),([1,2,4],[3]),([1,3],[2,4]),([1,3,4],[2]),([1,4],[2,3])]
-- >>> Set.size . combinations . Set.fromList $ [1,2,3,4]
-- 8
-- >>> Set.size (Set.powerSet (Set.fromList [1,2,3,4]))
-- 16
combinations :: Ord a => Set.Set a -> Set.Set (Set.Set a, Set.Set a)
combinations orig =
  Set.filter (uncurry (<)) . Set.map (\sub -> (sub, Set.difference orig sub)) $ Set.powerSet orig

maxPresssureRelease2 :: Map Text Valve -> Valve -> Int -> (Int, [Text])
maxPresssureRelease2 valves current remainingTime
  | remainingTime <= 0 = (0, [])
  | Map.size valves == 0 = (0, [])
  | otherwise =
      maximumOn fst . Map.elems $
        Map.mapWithKey
          ( \n v ->
              let travelAndOpenTime = (tunnels current Map.! n) + 1
                  t' = remainingTime - travelAndOpenTime
               in if t' > 0
                    then
                      let (pressure, trail) = maxPresssureRelease2 (Map.delete n valves) v t'
                       in (pressure + (flowRate v * t'), n : trail)
                    else (0, [])
          )
          valves

data Valve = Valve
  { flowRate :: Int,
    tunnels :: Map Text Int
  }
  deriving (Show)

flowRate_ :: Lens' Valve Int
flowRate_ f v = fmap (\t' -> v {flowRate = t'}) (f (flowRate v))

tunnels_ :: Lens' Valve (Map Text Int)
tunnels_ f v = fmap (\t' -> v {tunnels = t'}) (f (tunnels v))

-- >>> parseOnly nameParser "AA"
-- Right "AA"
nameParser :: Parser Text
nameParser = Text.pack <$> sequence [anyChar, anyChar]

-- >>> parseOnly valveParser "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB"
-- Right ("AA",Valve {flowRate = 0, tunnels = fromList [("BB",1),("DD",1),("II",1)]})
-- >>> parseOnly (valveParser <* endOfInput) "Valve GG has flow rate=0; tunnels lead to valves FF, HH"
-- Right ("GG",Valve {flowRate = 0, tunnels = fromList [("FF",1),("HH",1)]})
-- >>> parseOnly (valveParser <* endOfInput) "Valve HH has flow rate=22; tunnel leads to valve GG"
-- Right ("HH",Valve {flowRate = 22, tunnels = fromList [("GG",1)]})
valveParser :: Parser (Text, Valve)
valveParser = do
  _ <- string "Valve "
  name <- nameParser
  _ <- string " has flow rate="
  fr <- decimal
  _ <- string "; tunnels lead to valves " <|> string "; tunnel leads to valve "
  ts <- nameParser `sepBy` string ", "
  pure (name, Valve fr (Map.fromSet (const 1) $ Set.fromList ts))

readInput :: IO (Map Text Valve)
readInput = do
  input <- Text.getContents
  case parseOnly ((valveParser `sepBy` endOfLine) <* endOfInput) input of
    Left e -> error e
    Right xs -> pure $ Map.fromList xs
