{-# LANGUAGE OverloadedStrings #-}

module Day10 where

import Control.Monad (unless)
import Data.Attoparsec.Text
import Data.Function ((&))
import Data.Functor (($>))
import Data.Functor.Identity (Identity (..))
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.List.Extra (chunksOf)
import Data.Maybe (isNothing)
import qualified Data.Text.IO as Text
import State
import Lens

day10_1 :: IO ()
day10_1 = do
  is <- readInput
  let initCPU = CPU 1 1 Nothing is
      (terminated, strengths) = runState runWithCheckpoints initCPU
  putStrLn $ "Final Value: " <> show (registerX terminated)
  putStrLn $ "Clock: " <> show (clock terminated)
  putStrLn $ "Strengths: " <> show (sum strengths)

data CPU f = CPU
  { registerX :: Int,
    clock :: Int,
    execution :: f Execution,
    instructions :: [Instruction]
  }

registerX_ :: Lens' (CPU f) Int
registerX_ f cpu = fmap (\r' -> cpu {registerX = r'}) (f (registerX cpu))

clock_ :: Lens' (CPU f) Int
clock_ f cpu = fmap (\c' -> cpu {clock = c'}) (f (clock cpu))

execution_ :: Lens (CPU f) (CPU g) (f Execution) (g Execution)
execution_ f cpu = fmap (\e' -> cpu {execution = e'}) (f (execution cpu))

instructions_ :: Lens (CPU f) (CPU f) [Instruction] [Instruction]
instructions_ f cpu = fmap (\i' -> cpu {instructions = i'}) (f (instructions cpu))

data Execution = Execution {instruction :: Instruction, cycles :: Int}
  deriving (Show)

runWithCheckpoints :: State (CPU Maybe) [Int]
runWithCheckpoints = do
  terminated <- tickUntilCheckpoint
  atCheckpoint <- isCheckpoint . clock <$> get
  if atCheckpoint
    then do
      c <- clock <$> get
      x <- registerX <$> get
      if terminated
        then pure [c * x]
        else fmap ((c * x) :) runWithCheckpoints
    else pure []

tickUntilCheckpoint :: State (CPU Maybe) Bool
tickUntilCheckpoint = do
  tick
  terminated <- isTerminated <$> get
  atCheckpoint <- isCheckpoint . clock <$> get
  if terminated || atCheckpoint
    then pure terminated
    else tickUntilCheckpoint

isCheckpoint :: Int -> Bool
isCheckpoint n =
  (n - 20) `mod` 40 == 0

tick :: State (CPU Maybe) ()
tick = modify (execute . updateExecution)

isTerminated :: CPU Maybe -> Bool
isTerminated cpu = null (instructions cpu) && isNothing (execution cpu)

updateExecution :: CPU Maybe -> CPU Identity
updateExecution cpu =
  case execution cpu of
    Nothing ->
      case instructions cpu of
        [] -> error "Terminated."
        (i : is) ->
          cpu
            & execution_ .~ Identity (Execution i 0)
            & instructions_ .~ is
    Just (Execution i n) ->
      cpu
        & execution_ .~ Identity (Execution i n)

execute :: CPU Identity -> CPU Maybe
execute cpu = clock_ %~ (+ 1) $
  case runIdentity (execution cpu) of
    Execution add@(AddX _) 0 -> cpu & execution_ ?~ Execution add 1
    Execution (AddX n) 1 ->
      cpu
        & execution_ .~ Nothing
        & registerX_ %~ (+ n)
    Execution _ _ -> cpu & execution_ .~ Nothing

day10_2 :: IO ()
day10_2 = do
  is <- readInput
  let initCPU = CPU 1 1 Nothing is
      ((terminated, pixels), _) = runState runWithRendering (initCPU, mempty)
  putStrLn $ "Final Value: " <> show (registerX terminated)
  putStrLn $ "Clock: " <> show (clock terminated)
  putStrLn $ renderPixels pixels

renderPixels :: IntMap Bool -> String
renderPixels pixels =
  let grid = chunksOf 40 [1 .. 240]
      ls = map (map pixel) grid
      pixel i = case IntMap.lookup i pixels of
                  Nothing -> '!'
                  Just True -> '#'
                  Just False -> '.'
   in unlines ls

runWithRendering :: State (CPU Maybe, IntMap Bool) ()
runWithRendering = do
  c <- gets (^. _1 . clock_)
  x <- gets (^. _1 . registerX_)
  let pixel = (c - 1) `mod` 40
      isLit = (x - 1 == pixel) || x == pixel || (x + 1 == pixel)
  modify (_2 %~ IntMap.insert c isLit)
  fstState tick
  terminated <- gets $ isTerminated . fst
  let renderComplete = c == 240
  unless (terminated || renderComplete) runWithRendering

fstState :: State s a -> State (s, t) a
fstState (State f) =
  State
    ( \(s, t) ->
        let (s', a) = f s
         in ((s', t), a)
    )

data Instruction
  = AddX Int
  | Noop
  deriving (Show)

addxParser :: Parser Instruction
addxParser = string "addx " *> (AddX <$> signed decimal)

noopParser :: Parser Instruction
noopParser = string "noop" $> Noop

instructionParser :: Parser Instruction
instructionParser = choice [addxParser, noopParser]

readInput :: IO [Instruction]
readInput = do
  input <- Text.getContents
  case parseOnly ((instructionParser `sepBy` endOfLine) <* endOfInput) input of
    Left e -> error e
    Right is -> pure is
