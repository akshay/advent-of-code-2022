{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Day15 where

import Data.Attoparsec.Text
import Data.List (sort)
import Data.List.Extra (firstJust)
import Data.Maybe (listToMaybe, mapMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text.IO as Text
import System.Environment (getArgs)

day15_1 :: IO ()
day15_1 = do
  targetY <- read . head <$> getArgs
  pairs <- readInput
  let noBeacons = Set.unions $ map (uncurry $ noBeaconsFor targetY) pairs
      actualBeaconsInY = Set.fromList . map fst . filter (\(_, y) -> y == targetY) $ map snd pairs
      impossibleBeacons = Set.difference noBeacons actualBeaconsInY
  putStrLn $ "Impossible Positions: " <> show (Set.size impossibleBeacons)

noBeaconsFor :: Int -> Position -> Position -> Set Int
noBeaconsFor y (sx, sy) (bx, by) =
  let manhattanDistance = abs (by - sy) + abs (bx - sx)
      closestToY = abs (y - sy)
      remainingDistance = manhattanDistance - closestToY
   in if remainingDistance < 0
        then mempty
        else Set.fromList [(sx - remainingDistance) .. (sx + remainingDistance)]

day15_2 :: IO ()
day15_2 = do
  maxXY <- read . head <$> getArgs
  pairs <- readInput
  let minXY = 0
      impossibleXs y = mapMaybe (uncurry $ noBeaconsRange y) pairs
      firstPossibleX =
        fmap fst . listToMaybe . flipRanges minXY maxXY . sort . impossibleXs
      firstPossiblePosition =
        firstJust (\y -> (,y) <$> firstPossibleX y) [minXY .. maxXY]
  case firstPossiblePosition of
    Nothing ->
      error "No possible positions for the beacon"
    Just (bx, by) ->
      putStrLn $ "Possible Positions: " <> show (bx * 4000000 + by)

noBeaconsRange :: Int -> Position -> Position -> Maybe (Int, Int)
noBeaconsRange y (sx, sy) (bx, by) =
  let manhattanDistance = abs (by - sy) + abs (bx - sx)
      closestToY = abs (y - sy)
      remainingDistance = manhattanDistance - closestToY
   in if remainingDistance < 0
        then Nothing
        else Just (sx - remainingDistance, sx + remainingDistance)

-- | Assumes ordered list of ranges
--
-- >>> flipRanges 0 20 [(0,10), (15, 20)]
-- [(11,14)]
-- >>> flipRanges 0 20 []
-- [(0,20)]
-- >>> flipRanges 0 20 [(-1, 30)]
-- []
-- >>> flipRanges 0 20 [(-1, 14), (10, 19)]
-- [(20,20)]
-- >>> flipRanges 0 20 [(1, 19)]
-- [(0,0),(20,20)]
flipRanges :: Int -> Int -> [(Int, Int)] -> [(Int, Int)]
flipRanges minXY maxXY []
  | minXY <= maxXY = [(minXY, maxXY)]
  | otherwise = []
flipRanges minXY maxXY ((start, end) : rs)
  | end < minXY = flipRanges minXY maxXY rs
  | start > maxXY = [(minXY, maxXY)]
  | start <= minXY = flipRanges (end + 1) maxXY rs
  | otherwise = (minXY, start - 1) : flipRanges (end + 1) maxXY rs

type Position = (Int, Int)

sensorParser :: Parser Position
sensorParser = do
  _ <- string "Sensor at "
  positionParser <* string ": "

beaconParser :: Parser Position
beaconParser = do
  _ <- string "closest beacon is at "
  positionParser

positionParser :: Parser Position
positionParser = do
  _ <- "x="
  x <- signed decimal
  _ <- string ", y="
  y <- signed decimal
  pure (x, y)

readInput :: IO [(Position, Position)]
readInput = do
  input <- Text.getContents
  let pairParser = (,) <$> sensorParser <*> beaconParser
  case parseOnly ((pairParser `sepBy` endOfLine) <* endOfInput) input of
    Left e -> error e
    Right ps -> pure ps
