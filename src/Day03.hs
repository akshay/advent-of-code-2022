module Day03 where

import Data.Char
import Data.List (intersect, nub)

day3_1 :: IO ()
day3_1 = do
  rucksacks <- readInput
  let totalPriority = sum $ map (priority . sharedItemByCompartment) rucksacks
  putStrLn $ "Total Priority: " <> show totalPriority

-- | Assumes exactly one shared item, otherwise throws error
sharedItemByCompartment :: Rucksack -> Item
sharedItemByCompartment (comp1, comp2) =
  case comp1 `intersect` comp2 of
    [shared] -> shared
    xs -> error $ "Expected only one shared item, found: " <> show xs

priority :: Item -> Int
priority i =
  if isAsciiLower i
    then ord i - ord 'a' + 1
    else ord i - ord 'A' + 27

day3_2 :: IO ()
day3_2 = do
  rucksacks <- readInput
  let groups = groupRucksacks rucksacks
      totalPriority = sum $ map (priority . findBadge) groups
  putStrLn $ "Total Priority: " <> show totalPriority

type Group = (Rucksack, Rucksack, Rucksack)

-- | The number of rucksacks must be multiple of 3
groupRucksacks :: [Rucksack] -> [Group]
groupRucksacks [] = []
groupRucksacks (r1 : r2 : r3 : rest) = (r1, r2, r3) : groupRucksacks rest
groupRucksacks _ = error "not enough rucksacks to group"

-- | Assumes exactly one shared item, otherwise throws error
findBadge :: Group -> Item
findBadge ((c11, c12), (c21, c22), (c31, c32)) =
  case nub $ (c11 <> c12) `intersect` (c21 <> c22) `intersect` (c31 <> c32) of
    [shared] -> shared
    xs -> error $ "Expected only one shared item, found: " <> show xs

type Rucksack = (Compartment, Compartment)

type Compartment = [Item]

type Item = Char

readInput :: IO [Rucksack]
readInput = do
  inputStr <- getContents
  let rucksacksWithoutCompartments = lines inputStr
      rucksacks = map halve rucksacksWithoutCompartments
  pure rucksacks

halve :: [a] -> ([a], [a])
halve xs = splitAt (length xs `div` 2) xs
