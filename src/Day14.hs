{-# LANGUAGE OverloadedStrings #-}

module Day14 where

import Data.Attoparsec.Text
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.Text.IO as Text
import State

day14_1 :: IO ()
day14_1 = do
  rocks <- readInput
  let (_, restedUnits) = runState pourSand (IntMap.unionsWith Set.union $ map rockySpaces rocks)
  putStrLn $ "Rested sand units: " <> show restedUnits

type Blocked = IntMap (Set Int)

isBlocked :: Position -> Blocked -> Bool
isBlocked (i, j) =
  maybe False (Set.member j) . IntMap.lookup i

pourSand :: State Blocked Int
pourSand = go 0
  where
    go n = do
      (didFallForever, didBlockSource) <- dropSand (500, 0)
      if didFallForever || didBlockSource
        then pure (n + fromEnum didBlockSource)
        else go (n + 1)

dropSand :: Position -> State Blocked (Bool, Bool)
dropSand (i, j) = do
  blocked <- get
  let blockedJs = IntMap.lookup i blocked
      mBlockedBy = Set.lookupMin . Set.filter (>= j) =<< blockedJs
  case mBlockedBy of
    Nothing -> pure (True, False)
    Just jBlock -> do
      leftBlocked <- gets $ isBlocked (i - 1, jBlock)
      if not leftBlocked
        then dropSand (i - 1, jBlock)
        else do
          rightBlocked <- gets $ isBlocked (i + 1, jBlock)
          if not rightBlocked
            then dropSand (i + 1, jBlock)
            else do
              modify (IntMap.insertWith Set.union i (Set.singleton (jBlock - 1)))
              pure (False, (i, jBlock - 1) == (500, 0))

rockySpaces :: Rock -> Blocked
rockySpaces [] = mempty
rockySpaces [(i, j)] = IntMap.singleton i (Set.singleton j)
rockySpaces (p1 : p2 : rest) =
  IntMap.unionWith Set.union (line p1 p2) (rockySpaces (p2 : rest))

line :: Position -> Position -> Blocked
line (i1, j1) (i2, j2)
  | i1 == i2 = IntMap.singleton i1 (smallToLarge j1 j2)
  | j1 == j2 = Set.foldr (\i -> IntMap.insert i (Set.singleton j1)) mempty (smallToLarge i1 i2)
  | otherwise = error "Invalid line"

smallToLarge :: (Enum a, Ord a) => a -> a -> Set a
smallToLarge x y =
  Set.fromList $ if fromEnum x < fromEnum y then [x .. y] else [y .. x]

day14_2 :: IO ()
day14_2 = do
  rocks <- readInput
  let blockedSpaces = IntMap.unionsWith Set.union $ map rockySpaces rocks
      jMax = (+ 2) . Set.findMax . Set.unions $ IntMap.elems blockedSpaces
      iMin = 500 - jMax
      iMax = 500 + jMax
      blockedWithFloor = IntMap.unionWith Set.union (line (iMin, jMax) (iMax, jMax)) blockedSpaces
      (finalBlocked, restedUnits) = runState pourSand blockedWithFloor
  putStrLn $ showMap blockedWithFloor finalBlocked
  putStrLn $ "Rested sand units: " <> show restedUnits

showMap :: Blocked -> Blocked -> String
showMap rocks sandAndRocks =
  let iMin = fst $ IntMap.findMin sandAndRocks
      iMax = fst $ IntMap.findMax sandAndRocks
      jMin = Set.findMin . Set.unions $ IntMap.elems sandAndRocks
      jMax = Set.findMax . Set.unions $ IntMap.elems sandAndRocks
      ls = map mkLine [jMin .. jMax]
      mkLine j = map (mkChar j) [iMin .. iMax]
      mkChar j i
        | isBlocked (i, j) rocks = '#'
        | isBlocked (i, j) sandAndRocks = 'o'
        | otherwise = '.'
   in unlines ls

type Position = (Int, Int)

type Rock = [Position]

positionParser :: Parser Position
positionParser = (,) <$> decimal <* char ',' <*> decimal

rockParser :: Parser Rock
rockParser = positionParser `sepBy` string " -> "

readInput :: IO [Rock]
readInput = do
  input <- Text.getContents
  case parseOnly ((rockParser `sepBy` endOfLine) <* endOfInput) input of
    Left err -> error err
    Right rs -> pure rs
