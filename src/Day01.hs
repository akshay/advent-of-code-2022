{-# LANGUAGE ScopedTypeVariables #-}

module Day01 where

import Data.Attoparsec.Text
import Data.List (sortOn)
import qualified Data.Text.IO as Text

day1_1 :: IO ()
day1_1 = do
  bags <- readInput
  let bagSums = map sum bags
  putStrLn $ "Max calories: " <> show (maximum bagSums)

day1_2 :: IO ()
day1_2 = do
  bags <- readInput
  let bagSums = map sum bags
      top3Sum = sum (Prelude.take 3 (sortOn negate bagSums))
  putStrLn $ "Sum of top 3: " <> show top3Sum

type ElfBag = [Calories]

type Calories = Int

caloriesParser :: Parser Calories
caloriesParser = decimal

elfBagParser :: Parser ElfBag
elfBagParser = caloriesParser `sepBy` char '\n'

elfBagsParser :: Parser [ElfBag]
elfBagsParser = elfBagParser `sepBy` char '\n'

readInput :: IO [ElfBag]
readInput = do
  inputStr <- Text.getContents
  case parseOnly (elfBagsParser <* endOfInput) inputStr of
    Left err -> error $ "Failed to parse: " <> show err
    Right bags -> pure bags
