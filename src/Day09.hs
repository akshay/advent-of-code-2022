module Day09 where

import Data.List.NonEmpty (NonEmpty (..))
import Data.Set (Set)
import qualified Data.Set as Set
import State

day9_1 :: IO ()
day9_1 = do
  motions <- readInput
  let (finalEnv, _) = runState (mapM doMotion motions) (initEnv 1)
  print $ tailPositions finalEnv
  putStrLn $ "Tail Positions: \n" <> showPositions (tailPositions finalEnv)
  putStrLn $ "Total tails positions: " <> show (Set.size $ tailPositions finalEnv)

showPositions :: Set Position -> String
showPositions ps =
  let xs = Set.map fst ps
      ys = Set.map snd ps
      minX = minimum xs
      maxX = maximum xs
      minY = minimum ys
      maxY = maximum ys
      ls = map showLine [maxY, (maxY - 1) .. minY]
      showLine y = map (showPoint y) [minX .. maxX]
      showPoint y x =
        if Set.member (x, y) ps
          then if (x, y) == (0, 0) then 's' else '#'
          else '.'
   in unlines ls

data Env = Env
  { rope :: NonEmpty Position,
    tailPositions :: Set Position
  }
  deriving (Show)

initEnv :: Int -> Env
initEnv n = Env ((0, 0) :| replicate n (0, 0)) mempty

doMotion :: Motion -> State Env ()
doMotion (_, 0) = pure ()
doMotion (dir, n) = do
  moveHead dir
  followTail
  doMotion (dir, n - 1)

newHeadPos :: Direction -> Position -> Position
newHeadPos U (x, y) = (x, y + 1)
newHeadPos D (x, y) = (x, y - 1)
newHeadPos L (x, y) = (x - 1, y)
newHeadPos R (x, y) = (x + 1, y)

moveHead :: Direction -> State Env ()
moveHead dir =
  modify $ \env ->
    env
      { rope = let (hd :| rest) = rope env in newHeadPos dir hd :| rest
      }

-- >>> runState followTail (Env ((0,0) :| [(0,0)]) mempty)
-- (Env {rope = (0,0) :| [(0,0)], tailPositions = fromList [(0,0)]},())
-- >>> runState followTail (Env ((1,0) :| [(0,0)]) mempty)
-- (Env {rope = (1,0) :| [(0,0)], tailPositions = fromList [(0,0)]},())
-- >>> runState followTail (Env ((2,0) :| [(0,0)]) mempty)
-- (Env {rope = (2,0) :| [(1,0)], tailPositions = fromList [(1,0)]},())
-- >>> runState followTail (Env ((0,1) :| [(0,0)]) mempty)
-- (Env {rope = (0,1) :| [(0,0)], tailPositions = fromList [(0,0)]},())
-- >>> runState followTail (Env ((0,2) :| [(0,0)]) mempty)
-- (Env {rope = (0,2) :| [(0,1)], tailPositions = fromList [(0,1)]},())
-- >>> runState followTail (Env ((1,1) :| [(0,0)]) mempty)
-- (Env {rope = (1,1) :| [(0,0)], tailPositions = fromList [(0,0)]},())
-- >>> runState followTail (Env ((2,2) :| [(0,0)]) mempty)
-- (Env {rope = (2,2) :| [(1,1)], tailPositions = fromList [(1,1)]},())
-- >>> runState followTail (Env ((3,2) :| [(0,0)]) mempty)
-- (Env {rope = (3,2) :| [(1,1)], tailPositions = fromList [(1,1)]},())
-- >>> runState followTail (Env ((-3,-2) :| [(0,0)]) mempty)
-- (Env {rope = (-3,-2) :| [(-1,-1)], tailPositions = fromList [(-1,-1)]},())
-- >>> runState followTail (Env ((2,1) :| [(0,0)]) mempty)
-- (Env {rope = (2,1) :| [(1,1)], tailPositions = fromList [(1,1)]},())
-- >>> runState followTail (Env ((4,4) :| [(2,3), (1,2)]) mempty)
-- (Env {rope = (4,4) :| [(3,4),(2,3)], tailPositions = fromList [(2,3)]},())
followTail :: State Env ()
followTail =
  modify $ \env ->
    let h :| t = rope env
        newt = newTailsPositions h t
     in env
          { rope = h :| newt,
            tailPositions = Set.insert (last newt) (tailPositions env)
          }

newTailsPositions :: Position -> [Position] -> [Position]
newTailsPositions _ [] = []
newTailsPositions (hx, hy) ((tx, ty) : rest) =
  let newtx = if abs (hx - tx) > 1 || abs (hy - ty) > 1 then signum (hx - tx) + tx else tx
      newty = if abs (hx - tx) > 1 || abs (hy - ty) > 1 then signum (hy - ty) + ty else ty
   in (newtx, newty) : newTailsPositions (newtx, newty) rest

day9_2 :: IO ()
day9_2 = do
  motions <- readInput
  let (finalEnv, _) = runState (mapM doMotion motions) (initEnv 9)
  print $ tailPositions finalEnv
  putStrLn $ "Tail Positions: \n" <> showPositions (tailPositions finalEnv)
  putStrLn $ "Total tails positions: " <> show (Set.size $ tailPositions finalEnv)

type Motion = (Direction, Int)

type Position = (Int, Int)

data Direction = U | D | L | R
  deriving (Read)

readInput :: IO [Motion]
readInput = do
  let readLine l =
        case words l of
          [dir, count] -> (read dir, read count)
          _ -> error $ "Invalid motion: " <> show l
  map readLine . lines <$> getContents
