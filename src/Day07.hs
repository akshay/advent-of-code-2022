{-# LANGUAGE OverloadedStrings #-}

module Day07 where

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import Data.List (tails)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.IO as Text

day7_1 :: IO ()
day7_1 = do
  cmds <- readInput
  let sizes = dirSizes cmds
      atMost100000 = Map.filter (<= 100000) sizes
  putStrLn $ "Sum of at most 100000: " <> show (sum atMost100000)

-- Not really required
printSizes :: Map [Text] Int -> Text
printSizes =
  Map.foldMapWithKey (\path size -> "/" <> Text.intercalate "/" (reverse path) <> " -> " <> Text.pack (show size) <> "\n")

dirSizes :: [Command] -> Map [Text] Int
dirSizes = go [] mempty
  where
    go :: [Text] -> Map [Text] Int -> [Command] -> Map [Text] Int
    go _ sizes [] = sizes
    go cwd sizes ((CD "..") : rest) =
      case cwd of
        (_ : parent) -> go parent sizes rest
        _ -> go cwd sizes rest
    go _ sizes ((CD "/") : rest) =
      go [] sizes rest
    go cwd sizes ((CD dirName) : rest) =
      go (dirName : cwd) sizes rest
    go cwd sizes ((LS nodes) : rest) =
      let cwdSize = sum $ map nodeSize nodes
          newSizes = foldr (\dir -> Map.insertWith (+) dir cwdSize) sizes (tails cwd)
       in go cwd newSizes rest

day7_2 :: IO ()
day7_2 = do
  cmds <- readInput
  let sizes = dirSizes cmds
      totalDiskSize = 70000000
      requiredSize = 30000000
      usedSpace = sizes Map.! []
      deficit = requiredSize - (totalDiskSize - usedSpace)
      deletedSize = minimum $ Map.filter (>= deficit) sizes
  putStrLn $ "Size of directory to be deleted: " <> show deletedSize

data Command = CD Text | LS [Node]

data Node
  = DirName Text
  | FileSize Int Text

nodeSize :: Node -> Int
nodeSize (DirName _) = 0
nodeSize (FileSize s _) = s

cdParser :: Parser Command
cdParser =
  CD
    <$ string "cd "
    <*> takeTill (== '\n')

dirNodeParser :: Parser Node
dirNodeParser =
  DirName
    <$ string "dir "
    <*> takeTill (== '\n')

fileNodeParser :: Parser Node
fileNodeParser =
  FileSize
    <$> decimal
    <* char ' '
    <*> takeTill (== '\n')

nodeParser :: Parser Node
nodeParser = dirNodeParser <|> fileNodeParser

lsParser :: Parser Command
lsParser =
  LS
    <$ string "ls"
    <* endOfLine
    <*> nodeParser
    `sepBy` endOfLine

commandParser :: Parser Command
commandParser =
  string "$ " *> (cdParser <|> lsParser)

readInput :: IO [Command]
readInput = do
  input <- Text.getContents
  case parseOnly ((commandParser `sepBy` endOfLine) <* endOfInput) input of
    Left err -> error err
    Right cmds -> pure cmds
