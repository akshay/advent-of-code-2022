module Day12 where

import Control.Monad.Extra (unlessM)
import Data.Function ((&))
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Lens
import State

day12_1 :: IO ()
day12_1 = do
  (hm, start, end) <- readInput
  let env = mkEnv hm end
      (explored, _) = runState (fewestSteps (Set.singleton start)) env
      ((endSteps, _), _) = fromMaybe (error "Did not get anywhere") $ Map.minViewWithKey (ends explored)
  putStrLn $ "End Steps: " <> show endSteps

data Env = Env
  { ends :: Map Int (Set (Position, Char)),
    traversed :: Set Position,
    heightMap :: HeightMap
  }

mkEnv :: HeightMap -> Position -> Env
mkEnv h p =
  Env
    { ends = Map.singleton 0 (Set.singleton (p, 'z')),
      traversed = Set.singleton p,
      heightMap = h
    }

ends_ :: Lens' Env (Map Int (Set (Position, Char)))
ends_ f e = fmap (\c' -> e {ends = c'}) (f (ends e))

traversed_ :: Lens' Env (Set Position)
traversed_ f e = fmap (\c' -> e {traversed = c'}) (f (traversed e))

haveReachedAny :: Set Position -> State Env Bool
haveReachedAny ps =
  gets (not . Set.null . Set.intersection ps . traversed)

canMove :: Char -> Char -> Bool
canMove to from = to <= succ from

fewestSteps :: Set Position -> State Env ()
fewestSteps beginings = loop
  where
    loop = unlessM (haveReachedAny beginings) $ do
      ((distance, closestPoints), restEnds) <- gets (fromMaybe (error "No closest points!") . Map.minViewWithKey . ends)
      hm <- gets heightMap
      forbidden <- gets traversed
      let traversableNeighbours = Set.unions $ flip Set.map closestPoints $ \(point, height) ->
            let neighbours = Map.restrictKeys hm (neighbourPositions point `Set.difference` forbidden)
                interestingNeighbours = Map.filter (canMove height) neighbours
             in Set.fromList $ Map.toList interestingNeighbours
      modify $ \env ->
        env
          & ends_ .~ Map.insertWith (<>) (distance + 1) traversableNeighbours restEnds
          & traversed_ %~ Set.union (Set.map fst traversableNeighbours)
      loop

neighbourPositions :: Position -> Set Position
neighbourPositions (x, y) = Set.fromList [(x, y + 1), (x + 1, y), (x - 1, y), (x, y - 1)]

day12_2 :: IO ()
day12_2 = do
  (hm, _, end) <- readInput
  let env = mkEnv hm end
      as = Map.keysSet $ Map.filter (== 'a') hm
      (explored, _) = runState (fewestSteps as) env
      ((endSteps, _), _) = fromMaybe (error "Did not get anywhere") $ Map.minViewWithKey (ends explored)
  putStrLn $ "End Steps: " <> show endSteps

type HeightMap = Map Position Char

type Position = (Int, Int)

parseMap :: [[Char]] -> HeightMap
parseMap matrix =
  Map.fromList $ concat $ zipWith (\l i -> zipWith (\c j -> ((i, j), c)) l [0 ..]) matrix [0 ..]

readInput :: IO (HeightMap, Position, Position)
readInput = do
  matrix <- lines <$> getContents
  let m = parseMap matrix
      start = head . Map.keys $ Map.filter (== 'S') m
      end = head . Map.keys $ Map.filter (== 'E') m
      m' =
        Map.map
          ( \h -> case h of
              'S' -> 'a'
              'E' -> 'z'
              _ -> h
          )
          m
  pure (m', start, end)
