{-# LANGUAGE OverloadedStrings #-}

module Day05 where

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import Data.IntMap (IntMap)
import qualified Data.IntMap as IntMap
import Data.Maybe (mapMaybe)
import qualified Data.Text.IO as Text

day5_1 :: IO ()
day5_1 = do
  (initStacks, instructions) <- readInput
  let finalStacks = foldl executeInstruction9000 initStacks instructions
  putStrLn $ "Top Crates: " <> show (topCrates finalStacks)

executeInstruction9000 :: Stacks -> Instruction -> Stacks
executeInstruction9000 stacks (Instruction n from to) =
  let (moved, newFrom) = splitAt n (stacks IntMap.! from)
      newTo = reverse moved <> stacks IntMap.! to
   in IntMap.insert from newFrom $ IntMap.insert to newTo stacks

topCrates :: Stacks -> [Crate]
topCrates = IntMap.elems . IntMap.map head

day5_2 :: IO ()
day5_2 = do
  (initStacks, instructions) <- readInput
  let finalStacks = foldl executeInstruction9001 initStacks instructions
  putStrLn $ "Top Crates: " <> show (topCrates finalStacks)

executeInstruction9001 :: Stacks -> Instruction -> Stacks
executeInstruction9001 stacks (Instruction n from to) =
  let (moved, newFrom) = splitAt n (stacks IntMap.! from)
      newTo = moved <> stacks IntMap.! to
   in IntMap.insert from newFrom $ IntMap.insert to newTo stacks

type Crate = Char

type Stack = [Crate]

type Stacks = IntMap Stack

data Instruction = Instruction
  { numberOfCrates :: Int,
    fromStack :: Int,
    toStack :: Int
  }

readInput :: IO (Stacks, [Instruction])
readInput = do
  inputText <- Text.getContents
  case parseOnly (inputParser <* endOfInput) inputText of
    Left err -> error err
    Right i -> pure i

-- >>> parseOnly crateParser "   "
-- Right Nothing
-- >>> parseOnly crateParser "[A]"
-- Right (Just 'A')
crateParser :: Parser (Maybe Crate)
crateParser =
  Just <$> (char '[' *> anyChar <* char ']')
    <|> (Nothing <$ string "   ")

-- >>> parseOnly crateLineParser "[C] [Z]"
-- Right [Just 'C',Just 'Z']
-- >>> parseOnly crateLineParser "    [X]"
-- Right [Nothing,Just 'X']
-- >>> parseOnly crateLineParser "[V]    "
-- Right [Just 'V',Nothing]
crateLineParser :: Parser [Maybe Crate]
crateLineParser = crateParser `sepBy1` char ' '

-- >>> let lines = "[C] [Z]\n    [F]\n 1  2 " in parseOnly crateLinesParser lines
-- Right [[Just 'C',Just 'Z'],[Nothing,Just 'F']]
crateLinesParser :: Parser [[Maybe Crate]]
crateLinesParser = crateLineParser `sepBy1` endOfLine

-- >>> parseOnly stackNumberParser " 1 "
-- Right 1
stackNumberParser :: Parser Int
stackNumberParser = char ' ' *> decimal <* char ' '

-- >>> parseOnly stackNumbersParser " 1   2 "
-- Right [1]
stackNumbersParser :: Parser [Int]
stackNumbersParser = stackNumberParser `sepBy1` char ' '

-- >>> let stacks = "[C] [Z]\n    [F]\n 1   2 " in parseOnly stacksParser stacks
-- Right (fromList [(1,"C"),(2,"ZF")])
stacksParser :: Parser Stacks
stacksParser = do
  crateLines <- crateLinesParser
  _ <- char '\n'
  stackNumbers <- stackNumbersParser
  pure . IntMap.fromList $ zipWith (\stackNum i -> (stackNum, mkStack crateLines i)) stackNumbers [0 ..]

mkStack :: [[Maybe Crate]] -> Int -> Stack
mkStack crateMatrix i = mapMaybe (!! i) crateMatrix

instructionParser :: Parser Instruction
instructionParser =
  Instruction
    <$ string "move "
    <*> decimal
    <* string " from "
    <*> decimal
    <* string " to "
    <*> decimal

instructionsParser :: Parser [Instruction]
instructionsParser = instructionParser `sepBy1` endOfLine

inputParser :: Parser (Stacks, [Instruction])
inputParser =
  (,)
    <$> stacksParser
    <* many1 space
    <*> instructionsParser
