module Day08 where

import Data.Map (Map)
import qualified Data.Map as Map
import Data.Set (Set)
import qualified Data.Set as Set

day8_1 :: IO ()
day8_1 = do
  gridLists <- readInput
  let grid = mkGrid gridLists
  putStrLn "All Trees:"
  putStrLn $ printGrid grid
  putStrLn "Visible trees from edge: "
  putStrLn $ printSelect grid (visibleTreesFromEdge grid)
  putStrLn $ "Number of visible trees from edge: " <> show (Set.size $ visibleTreesFromEdge grid)

type Grid a = Map (Int, Int) a

printSelect :: Grid Int -> Set (Int, Int) -> String
printSelect grid coords =
  let selected = Map.restrictKeys grid coords
   in printGrid selected

printGrid :: Grid Int -> String
printGrid grid =
  let (maxRow, maxCol) = maximum (Map.keys grid)
      rows = map printRow [0 .. maxRow]
      printRow i = concatMap (printTree i) [0 .. maxCol]
      printTree i j = maybe "." show $ Map.lookup (i, j) grid
   in unlines rows

mkGrid :: [[a]] -> Grid a
mkGrid gridLists =
  foldr
    ( \(row, i) acc ->
        foldr (\(x, j) -> Map.insert (i, j) x) acc (zip row [0 ..])
    )
    mempty
    (zip gridLists [0 ..])

visibleTreesFromEdge :: Grid Int -> Trees
visibleTreesFromEdge grid =
  Map.foldrWithKey (\tree -> Set.union . visibleTreesFromTree grid tree) edgeTrees (Map.restrictKeys grid edgeTrees)
  where
    minRow = 0
    minCol = 0
    (maxRow, maxCol) = maximum (Map.keys grid)

    edgeTrees = foldr (Set.union . Set.fromList) mempty [topEdge, bottomEdge, leftEdge, rightEdge]

    topEdge = [(minRow, y) | y <- [minRow .. maxRow]]
    bottomEdge = [(maxRow, y) | y <- [minRow .. maxRow]]
    leftEdge = [(x, minCol) | x <- [minCol .. maxCol]]
    rightEdge = [(x, maxCol) | x <- [minCol .. maxCol]]

type Trees = Set (Int, Int)

visibleTreesFromTree :: Grid Int -> (Int, Int) -> Int -> Trees
visibleTreesFromTree grid orig height =
  foldr Set.union mempty $ visibleTreesFromTree' grid (-1) 9 orig height

visibleTreesFromTree' :: Grid Int -> Int -> Int -> (Int, Int) -> Int -> [Trees]
visibleTreesFromTree' grid viewingHeight maxHeight (rowOrig, colOrig) origHeight = [lookDown, lookUp, lookRight, lookLeft]
  where
    lookDown :: Set (Int, Int)
    lookDown =
      let rowFn = (+ 1)
          colFn = id
       in scan rowFn colFn (rowFn rowOrig) (colFn colOrig)

    lookUp :: Set (Int, Int)
    lookUp =
      let rowFn x = x - 1
          colFn = id
       in scan rowFn colFn (rowFn rowOrig) (colFn colOrig)

    lookRight :: Set (Int, Int)
    lookRight =
      let rowFn = id
          colFn = (+ 1)
       in scan rowFn colFn (rowFn rowOrig) (colFn colOrig)

    lookLeft :: Set (Int, Int)
    lookLeft =
      let rowFn = id
          colFn x = (x - 1)
       in scan rowFn colFn (rowFn rowOrig) (colFn colOrig)

    scan :: (Int -> Int) -> (Int -> Int) -> Int -> Int -> Set (Int, Int)
    scan nextRow nextCol = go origHeight
      where
        go :: Int -> Int -> Int -> Set (Int, Int)
        go highest row col
          | highest >= maxHeight = mempty
          | otherwise =
              case Map.lookup (row, col) grid of
                Nothing -> mempty
                Just tree ->
                  let change =
                        if tree > highest || tree < viewingHeight
                          then Set.insert (row, col)
                          else id
                   in change $ go (max highest tree) (nextRow row) (nextCol col)

day8_2 :: IO ()
day8_2 = do
  gridLists <- readInput
  let grid = mkGrid gridLists
  putStrLn $ "Max scenic score: " <> show (maximum (scenicScores grid))

scenicScores :: Grid Int -> Grid Int
scenicScores grid =
  Map.mapWithKey (scenicScore grid) grid

scenicScore :: Grid Int -> (Int, Int) -> Int -> Int
scenicScore grid tree height =
  product . map Set.size $ visibleTreesFromTree' grid height height tree (-1)

readInput :: IO [[Int]]
readInput =
  map (map (read . (: []))) . lines <$> getContents
