{-# LANGUAGE OverloadedStrings #-}
module Day13 where

import Control.Applicative ((<|>))
import Data.Attoparsec.Text
import Data.String (IsString(..))
import qualified Data.Text.IO as Text
import qualified Data.Text as Text
import qualified Data.Set as Set

day13_1 :: IO ()
day13_1 = do
  pairs <- readInput
  let comparisons = map (uncurry (<)) pairs
      sumOfCorrectIndices = sum $ map snd $ filter fst $ zip comparisons [1 ..] :: Int
  putStrLn $ "Sum of correct indices: " <> show sumOfCorrectIndices

day13_2 :: IO ()
day13_2 = do
  pairs <- readInput
  let allPackets = concatMap (\(x,y) -> [x,y]) pairs
      sorted = Set.fromList $ ["[[2]]", "[[6]]"] <> allPackets
      decoderKey = (Set.findIndex "[[2]]" sorted + 1) * (Set.findIndex "[[6]]" sorted + 1)
  putStrLn $ "Decoder key: " <> show decoderKey

data Packet
  = PacketInt Int
  | PacketList [Packet]

-- | For convinience, not safe!
instance IsString Packet where
  fromString str = case parseOnly packetParser (Text.pack str) of
    Left e -> error e
    Right p -> p

instance Eq Packet where
  PacketInt i == PacketInt j = i == j
  PacketList p1 == PacketList p2 = p1 == p2
  PacketInt i == p = PacketList [PacketInt i] == p
  p == PacketInt i = p == PacketList [PacketInt i]

instance Ord Packet where
  PacketInt i `compare` PacketInt j = i `compare` j
  PacketList p1 `compare` PacketList p2 = p1 `compare` p2
  PacketInt i `compare` p = PacketList [PacketInt i] `compare` p
  p `compare` PacketInt i = p `compare` PacketList [PacketInt i]

packetParser :: Parser Packet
packetParser =
  (PacketInt <$> decimal)
    <|> (PacketList <$> (char '[' *> sepBy packetParser (char ',') <* char ']'))

readInput :: IO [(Packet, Packet)]
readInput = do
  input <- Text.getContents
  let pairParser = (,) <$> packetParser <* endOfLine <*> packetParser <* endOfLine
      pairsParser = pairParser `sepBy` endOfLine
  case parseOnly (pairsParser <* endOfInput) input of
    Left e -> error e
    Right pairs -> pure pairs
