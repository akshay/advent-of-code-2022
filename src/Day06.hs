module Day06 where

import Data.List (nub)

day6_1 :: IO ()
day6_1 = do
  input <- getLine
  let processedChars =
        case input of
          a : b : c : rest ->
            findFirstMarker 4 [a,b, c] rest
          _ -> error $ "not enough input: " <> show input
  putStrLn $ "First marker found after: " <> show processedChars

findFirstMarker :: Int -> [Char] -> String -> Int
findFirstMarker _ _ [] = error "No marker found"
findFirstMarker n prefix (x : rest) =
  let potentialMarker = (prefix <> [x])
  in if length (nub potentialMarker) == length potentialMarker
    then n
    else findFirstMarker (n + 1) (drop 1 prefix <> [x]) rest

day6_2 :: IO ()
day6_2 = do
  input <- getLine
  let (prefix, rest) = splitAt 13 input
  let processedChars = findFirstMarker 14 prefix rest
  putStrLn $ "First marker found after: " <> show processedChars
