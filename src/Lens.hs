{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}

module Lens where

import Data.Functor.Const
import Data.Functor.Identity

type Lens s t a b = forall f. Functor f => (a -> f b) -> s -> f t

type Lens' s a = Lens s s a a

type LensLike f s t a b = (a -> f b) -> s -> f t

type LensLike' f s a = (a -> f a) -> s -> f s

(^.) :: s -> LensLike' (Const a) s a -> a
(^.) s lens = getConst $ lens Const s

(.~) :: LensLike Identity s t a b -> b -> s -> t
(.~) lens x s = runIdentity $ lens (\_ -> Identity x) s

(%~) :: LensLike Identity s t a b -> (a -> b) -> s -> t
(%~) lens f s = runIdentity $ lens (Identity . f) s

(<>~) :: Semigroup a => LensLike Identity s t a a -> a -> s -> t
(<>~) l x = (%~) l (<> x)

(+~) :: Num b => LensLike Identity s t b b -> b -> s -> t
(+~) l x = (%~) l (+ x)

(?~) :: LensLike Identity s t a (Maybe b) -> b -> s -> t
(?~) lens x s = runIdentity $ lens (\_ -> Identity $ Just x) s

_1 :: Lens (a, b) (c, b) a c
_1 f (x, y) = fmap (,y) (f x)

_2 :: Lens (a, b) (a, c) b c
_2 f (x, y) = fmap (x,) (f y)
