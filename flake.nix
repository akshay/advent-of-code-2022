{
  description = "Dev Setup";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = {nixpkgs, flake-utils, ...}:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        # Avoids unnecessary recompiles
        filteredSource = pkgs.lib.cleanSourceWith {
          src = ./.;
          filter = path: type:
            let baseName = baseNameOf (toString path);
            in pkgs.lib.cleanSourceFilter path type && !(
              baseName == "flake.nix" ||
              baseName == "flake.lock" ||
              baseName == "dist-newstyle" ||
              builtins.match "^cabal\.project\..*$" baseName != null ||
              baseName == "hls.sh" ||
              baseName == ".envrc" ||
              baseName == "hie.yaml" ||
              baseName == ".hlint.yaml" ||
              baseName == ".hspec" ||
              baseName == "ci"
            );
        };
        ghcOverrides = hself: hsuper: rec {
          aoc2022 =  pkgs.haskell.lib.overrideSrc (hsuper.callPackage ./default.nix {}) {
            src = filteredSource;
          };
        };
        ghc924Pkgs = pkgs.haskell.packages.ghc924.override {
          overrides = ghcOverrides;
        };
      in rec {
        packages = rec {
          dev-env = ghc924Pkgs.shellFor {
            packages = p: [p.aoc2022];
            buildInputs = [
              pkgs.haskellPackages.cabal-install
              (pkgs.haskell-language-server.override {supportedGhcVersions = ["924"];})
              # pkgs.haskell-language-server
              pkgs.cabal2nix
              pkgs.ormolu
            ];
          };
          aoc2022 = ghc924Pkgs.aoc2022;
        };
        defaultPackage = packages.dev-env;
    });
}
