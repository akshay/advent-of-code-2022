{ mkDerivation, attoparsec, base, containers, extra, lib, matrices
, text
}:
mkDerivation {
  pname = "aoc2022";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    attoparsec base containers extra matrices text
  ];
  executableHaskellDepends = [ base ];
  license = "unknown";
  mainProgram = "aoc2022";
}
